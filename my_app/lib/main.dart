import 'package:flutter/material.dart';
import 'package:my_app/screen/signup_screen.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'WEYT',
      debugShowCheckedModeBanner: false,
      home: SignupScreen(),
    );
  }
}
